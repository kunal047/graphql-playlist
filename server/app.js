const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema/schema');
const mongoose = require('mongoose');

const app = express();

// connect to mlab database
mongoose.connect("mongodb://root:test123@ds149885.mlab.com:49885/learn-gql");
mongoose.connection.once('open', () => {
    console.log("Connected to database");
});

app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}) )

app.listen(4000, () => {
    console.log("Now listening for request on port 4000");
});
